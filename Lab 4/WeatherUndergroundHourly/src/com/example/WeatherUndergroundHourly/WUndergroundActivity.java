package com.example.WeatherUndergroundHourly;

import android.app.Activity;
import android.os.Bundle;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.media.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.util.Scanner;
import java.lang.Object;

import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import org.json.*;
import java.io.*;

public class WUndergroundActivity extends Activity implements LocationListener {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        setContentView(R.layout.main);

        LocationManager location = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        location.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        String[] citystate = new String[2];
        citystate[0] = "Philadelphia";
        citystate[1] = "PA";

        try{
            getWeather(citystate);
        }catch(Exception e)
        {

        }
    }

    public void onLocationChanged(Location location) {
        double latitude = (location.getLatitude());
        double longitude = (location.getLongitude());

        Log.i("Geo_Location", "Latitude: " + latitude + ", Longitude: " + longitude);
    }

    public Double[] onLocationChange(Location location) {

        double latitude = (location.getLatitude());
        double longitude = (location.getLongitude());

        Log.i("Geolocation", "Latitude: " + latitude + ", Longitude: " + longitude);

        Double[] coords = new Double[2];
        coords[0] = latitude;
        coords[1] = longitude;

        return coords;
    }

    public void getWeather(String[] citystate) throws Exception {

        StringBuilder sb = new StringBuilder();
        sb.append("http://api.wunderground.com/api/5358865cc04d1338/hourly/q/");
        sb.append(citystate[1]);
        sb.append(",");
        sb.append(citystate[0].replace(' ', '_'));
        sb.append(".json");

        URL hourlyURL = new URL(sb.toString());

        HttpURLConnection connection = (HttpURLConnection) hourlyURL.openConnection();
        connection.connect();

        BufferedReader dataIn = new BufferedReader(new InputStreamReader(hourlyURL.openStream()));

        StringBuilder response = new StringBuilder();
        String line;

        while ((line = dataIn.readLine()) != null) {
            response.append(line);
        }

        JSONObject hourlyObject = new JSONObject(response.toString());
        JSONArray responseHourly = hourlyObject.getJSONArray("hourly_forecast");

        RelativeLayout RL = (RelativeLayout) findViewById(R.id.results);
        setContentView(RL);

        int i = 1;

        for (Object item : (Iterable) responseHourly) {
            i++;
            JSONObject Date_Time = ((JSONObject)item).getJSONObject("FCTTIME");
            String month = Date_Time.getString("mon_padded");
            String day = Date_Time.getString("mday_padded");
            String hour = Date_Time.getString("hour");
            JSONObject tempJSON = ((JSONObject)item).getJSONObject("temp");
            String temp = tempJSON.getString("english");
            String current = ((JSONObject)item).getString("condition");
            String humidity = ((JSONObject)item).getString("humidity");
            String imageaddress = ((JSONObject)item).getString("icon_url");

            URL imageURL = new URL(imageaddress);

            String formattedweatherstring = String.format("Date: {0} \n" +
                    "\t Hour: {1} \n" +
                    "\t Temp: {2} \n" +
                    "\t Condition: {3} \n" +
                    "\t Humidity: {4}\n", month + " " + day, hour, temp, current, humidity);

            String bufferstring = "=======================================";

            android.widget.TextView hourPane = new android.widget.TextView(this);
            hourPane.setText(formattedweatherstring);

            RL.addView(hourPane);

            android.widget.ImageView icon = new android.widget.ImageView(this);

            switch (current) {
                case "Clear": icon.setImageResource(R.drawable.clear);
                    break;
                case "Cloudy": icon.setImageResource(R.drawable.cloudy);
                    break;
                case "Partly Cloudy": icon.setImageResource(R.drawable.partlycloudy);
                    break;
                case "Rain": icon.setImageResource(R.drawable.rain);
                    break;
                case "Snow": icon.setImageResource(R.drawable.snow);
                    break;
                case "Thunderstorm": icon.setImageResource(R.drawable.tstorm);
                    break;
                default: icon.setImageResource(R.drawable.ic_launcher);
                    break;
            }

            RL.addView(icon);

            android.widget.TextView buffer = new android.widget.TextView(this);
            buffer.setText(bufferstring);

            RL.addView(buffer);
        }
    }

    public String[] getCityAndState(double latitude, double longitude) throws Exception {

        String[] citystate = new String[2];

        String latitudestring = Double.toString(latitude);
        String longitudestring = Double.toString(longitude);

        StringBuilder sb = new StringBuilder();
        sb.append("http://api.wunderground.com/api/5358865cc04d1338/geolookup/q/");
        sb.append(latitudestring);
        sb.append(",");
        sb.append(longitudestring);
        sb.append(".json");

        String geolookupstring = sb.toString();

        URL url = new URL(geolookupstring);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.connect();

        BufferedReader dataIn = new BufferedReader(new InputStreamReader(url.openStream()));

        StringBuilder response = new StringBuilder();
        String line;

        while ((line = dataIn.readLine()) != null) {
            response.append(line);
        }

        JSONObject responseObject = new JSONObject(response.toString());
        JSONObject location = responseObject.getJSONObject("location");

        String city = location.getString("city");
        String state = location.getString("state");

        citystate[0] = city;
        citystate[1] = state;

        return citystate;
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
}
