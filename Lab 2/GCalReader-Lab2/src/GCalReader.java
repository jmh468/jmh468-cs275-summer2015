import java.io.*;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.temboo.core.*;
import com.temboo.Library.Google.*;
import com.temboo.Library.Google.OAuth.*;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Google.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Google.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Google.Calendar.*;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsInputSet;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsResultSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsInputSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsResultSet;

public class GCalReader
{
	public static void main(String[] args) throws Exception
	{
		TembooSession session = new TembooSession("jasonhickey2", "GCalReader-Lab2", "IoF6Z2cgEgsrIjPztCK3hc4y99sn07qZ");

		InitializeOAuth GCalOAuthInit = new InitializeOAuth(session);

		InitializeOAuthInputSet GCalOAuthInitInputs = GCalOAuthInit.newInputSet();

		GCalOAuthInitInputs.set_Scope("https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/calendar.readonly");
		GCalOAuthInitInputs.set_ClientID("511388759602-2il196qa6j0pjak8sm0akn0j7s4rup0h.apps.googleusercontent.com");
		GCalOAuthInitInputs.setCredential("GCalOAuth");
		
		InitializeOAuthResultSet GCalOAuthInitResult = GCalOAuthInit.execute(GCalOAuthInitInputs);
		
		System.out.println(GCalOAuthInitResult.get_AuthorizationURL());
		
		FinalizeOAuth GCalOAuthFinal = new FinalizeOAuth(session);
		
		FinalizeOAuthInputSet GCalOAuthFinalInputs = GCalOAuthFinal.newInputSet();
		
		GCalOAuthFinalInputs.set_ClientID("511388759602-2il196qa6j0pjak8sm0akn0j7s4rup0h.apps.googleusercontent.com");
		GCalOAuthFinalInputs.set_ClientSecret("bjF0gMpooE-Y3YmbzDWq9EmE");
		GCalOAuthFinalInputs.set_CallbackID(GCalOAuthInitResult.get_CallbackID());
		
		FinalizeOAuthResultSet GCalOAuthFinalResult = GCalOAuthFinal.execute(GCalOAuthFinalInputs);
		
		String accesstoken = GCalOAuthFinalResult.get_AccessToken();
		String refreshtoken = GCalOAuthFinalResult.get_RefreshToken();
		
		GetAllCalendars GCalgetcalendars = new GetAllCalendars(session);
		
		GetAllCalendarsInputSet GCalgetcalendarsInput = GCalgetcalendars.newInputSet();
		
		GCalgetcalendarsInput.set_AccessToken(accesstoken);
		GCalgetcalendarsInput.set_ResponseFormat("json");
		
		GetAllCalendarsResultSet GCalgetcalendarsResult = GCalgetcalendars.execute(GCalgetcalendarsInput);
		
		String getCalresponse = GCalgetcalendarsResult.get_Response();
		JSONObject getCalJSON = new JSONObject(getCalresponse);
		JSONArray calendars = getCalJSON.getJSONArray("items");
		ArrayList<String> calendartitles = new ArrayList<String>();
		ArrayList<String> calendarIDs = new ArrayList<String>();
		
		System.out.println("Calendars found: ");
		
		for (int i = 0; i < calendars.length(); i++)
		{
			JSONObject calendar = calendars.getJSONObject(i);
			String calendarname = calendar.getString("summary");
			String calendarID = calendar.getString("id");
			calendartitles.add(calendarname);
			calendarIDs.add(calendarID);
			System.out.println("\t" + calendarname);
		}
		
		System.out.println("==================================");
		
		GetAllEvents GCalgetevents = new GetAllEvents(session);
		GetAllEventsInputSet GCalgeteventsInput = GCalgetevents.newInputSet();
		
		GCalgeteventsInput.set_AccessToken(accesstoken);
		GCalgeteventsInput.set_ResponseFormat("json");
		GCalgeteventsInput.set_ClientID("511388759602-2il196qa6j0pjak8sm0akn0j7s4rup0h.apps.googleusercontent.com");
		GCalgeteventsInput.set_ClientSecret("bjF0gMpooE-Y3YmbzDWq9EmE");
		GCalgeteventsInput.set_CalendarID(calendarIDs.get(0).toString());
			
		GetAllEventsResultSet GCalgeteventsResult = GCalgetevents.execute(GCalgeteventsInput);
			
		String getEventsresponse = GCalgeteventsResult.get_Response();
		
		JSONObject getEventsJSON = new JSONObject(getEventsresponse);
		JSONArray events = getEventsJSON.getJSONArray("items");
		
		System.out.println("Events in " + calendartitles.get(0).toString() + ": " + events.length());
		System.out.println("Events: ");
		
		for (int i = 0; i < events.length(); i++)
		{
			JSONObject eventobject = events.getJSONObject(i);
			String title = eventobject.getString("summary");
			JSONObject startobject = eventobject.getJSONObject("start");
			String start = startobject.getString("dateTime");
			JSONObject endobject = eventobject.getJSONObject("end");
			String end = endobject.getString("dateTime");
			
			System.out.println("=======================================");
			System.out.println("Title: " + title);
			System.out.println("Start date/time: " + start);
			System.out.println("End date/time: " + end);
			System.out.println("=======================================");
		}
		
		System.out.println("Yes, I am aware that the first event is \'Event A\' while the others are \'2\' and \'3\'.");
		System.out.println("I'm leaving it that way as a silent act of rebellion against the Gregorian Calendar.");
	}
}
