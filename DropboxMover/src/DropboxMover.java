import java.io.*;

import org.json.JSONArray;
import org.json.JSONObject;

import com.enterprisedt.net.ftp.*;
import com.temboo.core.TembooSession;
import com.temboo.Library.Dropbox.OAuth.*;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.*;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileResultSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.SearchFilesAndFolders.SearchFilesAndFoldersInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.SearchFilesAndFolders.SearchFilesAndFoldersResultSet;

import it.sauronsoftware.*;
import it.sauronsoftware.ftp4j.*;
//import com.temboo.Library.Dropbox.FileOperations
import it.sauronsoftware.ftp4j.FTPClient;

public class DropboxMover 
{
	public static void main(String[] args) throws Exception
	{
		String username = "jmh468";
		String password = "e7bes9sd";
		
		// INSTANTIATE TEMBOO SESSION
		TembooSession session = new TembooSession("jasonhickey2", "DropboxMover", "tbseXUaS7irv2l1IAz6sEn1KEWB13Bsm");
		
		// INITIALIZE OAUTH PROCESS
		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);
		
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();
		
		initializeOAuthInputs.setCredential("dropboxoauth");
		initializeOAuthInputs.set_DropboxAppKey("0xov6uwr9l6z9gn");
		initializeOAuthInputs.set_DropboxAppSecret("vxbfpny8tacachw");
		
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
		
		String AuthURL = initializeOAuthResults.get_AuthorizationURL();
		
		System.out.println("Go to this URL to authorize: " + AuthURL);
		System.out.println("Press enter to continue after authorizing: ");
		System.in.read();
		
		// FINALIZE OAUTH PROCESS
		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);
		
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();
		
		finalizeOAuthInputs.set_OAuthTokenSecret(initializeOAuthResults.get_OAuthTokenSecret()); // "LGkMjoALvx47MQYf"
		finalizeOAuthInputs.set_DropboxAppSecret("vxbfpny8tacachw"); // "vxbfpny8tacachw"
		finalizeOAuthInputs.set_DropboxAppKey("0xov6uwr9l6z9gn"); // "0xov6uwr9l6z9gn"
		finalizeOAuthInputs.set_CallbackID(initializeOAuthResults.get_CallbackID()); // "1f4f9e27-3d21-4922-8fc9-8bc57bd11f7b"
		
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		
		// GET RESULTS OF SUCCESSFUL OAUTH
		String access_token = finalizeOAuthResults.get_AccessToken();
		String access_token_secret = finalizeOAuthResults.get_AccessTokenSecret();
		String user_ID = finalizeOAuthResults.get_UserID();
		
		// USE GETFILE CHOREO TO GET THE FILES
		// RETURNS FILE CONTENTS AS STRING
		// DON'T GET METADATA, BECAUSE IT DOESN'T MATTER RIGHT NOW
		
		SearchFilesAndFolders getpath = new SearchFilesAndFolders(session);
		
		SearchFilesAndFoldersInputSet getpathInput = getpath.newInputSet();
		getpathInput.set_AccessToken(access_token);
		getpathInput.set_AccessTokenSecret(access_token_secret);
		getpathInput.set_AppKey("0xov6uwr9l6z9gn");
		getpathInput.set_AppSecret("vxbfpny8tacachw");
		getpathInput.set_Query("_list");
		
		SearchFilesAndFoldersResultSet getpathResult = getpath.execute(getpathInput);
		
		JSONArray getpathResult_jsonArray = new JSONArray(getpathResult.get_Response());
		
		JSONObject getpathResult_json = getpathResult_jsonArray.getJSONObject(0);
		
		String path = getpathResult_json.getString("path");
		
		GetFile getListFileChoreo = new GetFile(session);
		
		GetFileInputSet getListFileInputs = getListFileChoreo.newInputSet();
		
		// Get paths from file
		getListFileInputs.set_AccessToken(access_token);
		getListFileInputs.set_AccessTokenSecret(access_token_secret);
		getListFileInputs.set_AppKey("0xov6uwr9l6z9gn");
		getListFileInputs.set_AppSecret("vxbfpny8tacachw");
		getListFileInputs.set_IncludeMetadata(false);
		getListFileInputs.set_EncodeFileContent(false);
		getListFileInputs.set_Path(path);
		
		GetFileResultSet getListFileResult = getListFileChoreo.execute(getListFileInputs);
		
		GetFile getActualFileChoreo = new GetFile(session);
		
		String[] fileContents = new String[2];
		
		InputStream is = new ByteArrayInputStream(getListFileResult.get_Response().getBytes());
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		
		String line;
		String[] addresses = new String[2];
		String[] filenames = {"A.txt", "B.txt"};
		int i = 0;
		
		// for each path in result
		while ((line = br.readLine()) != null)
		{	
			String[] paths = line.split("\\s+");
			System.out.println(paths[0]);
			addresses[i] = paths[1];
			
			SearchFilesAndFolders getpathoffile = new SearchFilesAndFolders(session);
			
			SearchFilesAndFoldersInputSet getpathoffileInput = getpathoffile.newInputSet();
			getpathoffileInput.set_AccessToken(access_token);
			getpathoffileInput.set_AccessTokenSecret(access_token_secret);
			getpathoffileInput.set_AppKey("0xov6uwr9l6z9gn");
			getpathoffileInput.set_AppSecret("vxbfpny8tacachw");
			getpathoffileInput.set_Query(filenames[i]);
			
			SearchFilesAndFoldersResultSet getpathoffileResult = getpathoffile.execute(getpathoffileInput);
			
			JSONArray getpathoffileResult_jsonArray = new JSONArray(getpathoffileResult.get_Response());
			
			JSONObject getpathoffileResult_json = getpathoffileResult_jsonArray.getJSONObject(0);
			
			String pathoffile = getpathoffileResult_json.getString("path");
			
			GetFileInputSet getFileInputs = getActualFileChoreo.newInputSet();
			
			getFileInputs.set_AccessToken(access_token);
			getFileInputs.set_AccessTokenSecret(access_token_secret);
			getFileInputs.set_AppKey("0xov6uwr9l6z9gn");
			getFileInputs.set_AppSecret("vxbfpny8tacachw");
			getFileInputs.set_IncludeMetadata(false);
			getFileInputs.set_Path(pathoffile);
			
			GetFileResultSet getActualFileResults = getActualFileChoreo.execute(getFileInputs);
			fileContents[i] = getActualFileResults.get_Response();
			i++;
		}
		
		br.close();
		
		PrintWriter pwFile1 = new PrintWriter("/Users/jasonhickey/Desktop/A.txt", "UTF-8");
		pwFile1.print(fileContents[0]);
		pwFile1.close();
		
		PrintWriter pwFile2 = new PrintWriter("/Users/jasonhickey/Desktop/B.txt", "UTF-8");
		pwFile2.print(fileContents[1]);
		pwFile2.close();
		
		// FTP THE FILES TO DREXEL DIRECTORIES
		FTPClient client = new FTPClient();
		client.connect("tux.cs.drexel.edu", 22);
		
		client.login(username, password);
		client.changeDirectory(addresses[0]);
		
		client.upload(new java.io.File("/Users/jasonhickey/Desktop/A.txt"));
		
		client.changeDirectory(addresses[1]);
		client.upload(new java.io.File("/Users/jasonhickey/Desktop/B.txt"));
	}
}