import java.lang.*;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.*;
import com.temboo.core.*;
import com.temboo.Library.Utilities.JSON.*;
import com.temboo.Library.Utilities.JSON.GetValuesFromJSON.GetValuesFromJSONInputSet;
import com.temboo.Library.Utilities.JSON.GetValuesFromJSON.GetValuesFromJSONResultSet;
import com.temboo.Library.Wordnik.*;
import com.temboo.Library.Wordnik.Account.GetAuthToken;
import com.temboo.Library.Wordnik.Account.GetAuthToken.GetAuthTokenInputSet;
import com.temboo.Library.Wordnik.Account.GetAuthToken.GetAuthTokenResultSet;
import com.temboo.Library.Wordnik.Word.*;
import com.temboo.Library.Wordnik.Word.GetHyphenation.GetHyphenationInputSet;
import com.temboo.Library.Wordnik.Word.GetHyphenation.GetHyphenationResultSet;
import com.temboo.Library.Twitter.*;
import com.temboo.Library.Twitter.OAuth.*;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineInputSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineResultSet;
import com.temboo.Library.Utilities.Validation.JSON;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;

public class TweetReader
{
	public static void main(String[] args) throws Exception
	{
		TembooSession session = new TembooSession("jasonhickey2", "TweetReader-Midterm", "CoajWyQ19N9RlPPtPG1jVVnNyJKdYIFR");
		
		// <TWITTER OAUTH>
		InitializeOAuth initOAuthTwitter = new InitializeOAuth(session);
		
		InitializeOAuthInputSet initOAuthTwitterInputs = initOAuthTwitter.newInputSet();
		
		initOAuthTwitterInputs.setCredential("TweetReaderOAuth");
		initOAuthTwitterInputs.set_ConsumerKey("lPCLNSjzu5AhSXj5uiLgOyPzR");
		initOAuthTwitterInputs.set_ConsumerSecret("fnDLg6MzZ4mkJFpOnNwUpYER3NmcT2upZRJ04PJ4gL3F1F1Btu");
		
		InitializeOAuthResultSet initOAuthTwitterResults = initOAuthTwitter.execute(initOAuthTwitterInputs);
		
		FinalizeOAuth finalOAuthTwitter = new FinalizeOAuth(session);
		
		System.out.println(initOAuthTwitterResults.get_AuthorizationURL());

		FinalizeOAuthInputSet finalOAuthTwitterInputs = finalOAuthTwitter.newInputSet();

		finalOAuthTwitterInputs.setCredential("TweetReaderOAuth");
		finalOAuthTwitterInputs.set_ConsumerKey("lPCLNSjzu5AhSXj5uiLgOyPzR");
		finalOAuthTwitterInputs.set_ConsumerSecret("fnDLg6MzZ4mkJFpOnNwUpYER3NmcT2upZRJ04PJ4gL3F1F1Btu");
		finalOAuthTwitterInputs.set_OAuthTokenSecret(initOAuthTwitterResults.get_OAuthTokenSecret());
		finalOAuthTwitterInputs.set_CallbackID(initOAuthTwitterResults.get_CallbackID());

		FinalizeOAuthResultSet finalOAuthTwitterResults = finalOAuthTwitter.execute(finalOAuthTwitterInputs);
		
		String accesstoken = finalOAuthTwitterResults.get_AccessToken();
		String accesstokenSecret = finalOAuthTwitterResults.get_AccessTokenSecret();
		String myscreenname = finalOAuthTwitterResults.get_ScreenName();
		//int userID = Integer.parseInt(finalOAuthTwitterResults.get_UserID());
		// </TWITTER OAUTH>
		
		// <GET TWEETS>
		String userScreenName = "burnie";
		
		UserTimeline timeline = new UserTimeline(session);
		
		UserTimelineInputSet timelineInputs = timeline.newInputSet();
		
		timelineInputs.set_AccessToken(accesstoken);
		timelineInputs.set_AccessTokenSecret(accesstokenSecret);
		timelineInputs.set_ConsumerKey("lPCLNSjzu5AhSXj5uiLgOyPzR");
		timelineInputs.set_ConsumerSecret("fnDLg6MzZ4mkJFpOnNwUpYER3NmcT2upZRJ04PJ4gL3F1F1Btu");
		timelineInputs.set_ScreenName(userScreenName);
		timelineInputs.set_Count(45);
		
		UserTimelineResultSet timelineResults = timeline.execute(timelineInputs);
		
		ArrayList<String> tweets = new ArrayList<String>();
		String response = timelineResults.get_Response();
		JSONArray responseObject = new JSONArray(response);
		
		//GetValuesFromJSON getvalues = new GetValuesFromJSON(session);
		//GetValuesFromJSONInputSet getvaluesinput = getvalues.newInputSet();
		//getvaluesinput.set_JSON(response);
		//getvaluesinput.set_Property("text");
		
		//GetValuesFromJSONResultSet getvaluesresult = getvalues.execute(getvaluesinput);
		
		for (int i = 0; i < responseObject.length(); i++)
		{
			//JSONObject objects = responseArray.getJSONObject(i);
			//tweets[i] = objects.getString("text");
			JSONObject tweetobject = responseObject.getJSONObject(i);
			String tweet = tweetobject.getString("text");
			tweets.add(tweet);
		}
		
		for (int i = 0; i < tweets.size(); i++)
		{
			System.out.println(tweets.get(i));
		}
		// </GET TWEETS>
		
		// <WORDNIK>
		ArrayList<String> words = new ArrayList<String>();
		String[] splitsentence;
		
		for (int i = 0; i < tweets.size(); i++)
		{
			splitsentence = tweets.get(i).split("\\s+");
			
			for (int j = 0; j < splitsentence.length; j++)
			{
				words.add(splitsentence[j]);
			}
		}
		
		int numSyllables;
		String numSyllablesString;
		ArrayList<Integer> syllableCounts = new ArrayList<Integer>();
		
		GetAuthToken wordnikAuth = new GetAuthToken(session);
		GetAuthTokenInputSet wordnikAuthInputs = wordnikAuth.newInputSet();
		
		wordnikAuthInputs.set_APIKey("010440fcb294b21a09005042caa09796dcce2b6bf78bd9c85");
		wordnikAuthInputs.set_Username("jasonhickey");
		wordnikAuthInputs.set_Password("password");
		
		GetAuthTokenResultSet wordnikAuthResults = wordnikAuth.execute(wordnikAuthInputs);

		String wordnikToken = wordnikAuthResults.get_Token();
		
		GetHyphenation getHyphenation = new GetHyphenation(session);
		
		GetHyphenationInputSet getHyphenationInputs = getHyphenation.newInputSet();
		
		GetHyphenationResultSet getHyphenationResults;
		
		for (int i = 0; i < words.size(); i++)
		{
			getHyphenationInputs.set_APIKey("010440fcb294b21a09005042caa09796dcce2b6bf78bd9c85");
			getHyphenationInputs.setCredential("wordnikAuth");
			getHyphenationInputs.set_UseCanonical(false);
			getHyphenationInputs.set_Word(words.get(i));
			
			try
			{
			getHyphenationResults = getHyphenation.execute(getHyphenationInputs);
			}
			catch(Exception ex)
			{
				break;
			}
			
			numSyllablesString = getHyphenationResults.get_Response();
			try
			{
				numSyllables = Integer.parseInt(numSyllablesString);
			}
			catch (NumberFormatException e)
			{
				numSyllables = 0;
			}
			syllableCounts.add(numSyllables);
		}
		// </WORDNIK>
		
		// <SMOG SCORE>
		double SMOGgrade;
		ArrayList<Integer> polysyllables = new ArrayList<Integer>();
		
		for (int i = 0; i < syllableCounts.size(); i++)
		{
			if (syllableCounts.get(i) >= 3)
			{
				polysyllables.add(syllableCounts.get(i));
			}
		}
		
		SMOGgrade = 1.0430 * Math.sqrt(polysyllables.size() * (30 / 45)) + 3.1291;
		
		System.out.println("SMOG score for @" + userScreenName + ": " + SMOGgrade);
		// </SMOG SCORE>
	}
}
