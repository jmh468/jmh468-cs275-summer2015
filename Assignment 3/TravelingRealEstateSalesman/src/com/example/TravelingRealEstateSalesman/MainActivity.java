package com.example.TravelingRealEstateSalesman;

import android.app.Activity;
import android.os.Bundle;
import android.view.*;
import android.content.*;
import android.widget.EditText;
import javax.xml.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.*;
import com.temboo.Library.Zillow.GetDeepSearchResults;
import com.temboo.core.*;
import com.temboo.Library.Google.Directions.*;
import com.temboo.Library.Zillow.GetDeepSearchResults.*;
import com.temboo.Library.Zillow.GetComps.*;
import com.temboo.Library.Zillow.GetComps.GetCompsInputSet;
import com.temboo.Library.Zillow.GetComps.GetCompsResultSet;
import com.temboo.Library.Utilities.XML.*;
import com.temboo.Library.Utilities.XML.GetValuesFromXML.*;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends Activity {
    public final static String EXTRA_MESSAGE = "com.example.TravelingRealEstateSalesman.MESSAGE";

    private GoogleMap GMap;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        GMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

        GMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
    }

    // finds open houses and puts them on the map
    public void searchTimes(View view) throws Exception
    {
        TembooSession session = new TembooSession("jasonhickey2", "TravelingRealEstateSalesman", "Tg67zZSNIFfGy383b25tzNrgvpQWJCdg");

        GetDeepSearchResults getinitialhouse = new GetDeepSearchResults(session);

        GetDeepSearchResultsInputSet getinitialhouseInput = getinitialhouse.newInputSet();
        getinitialhouseInput.setCredential("getInitialHouse");
        getinitialhouseInput.set_Address("3003 W Girard Ave");
        getinitialhouseInput.set_City("Philadelphia");
        getinitialhouseInput.set_State("PA");
        getinitialhouseInput.set_Zipcode(19130);
        getinitialhouseInput.set_ZWSID("X1-ZWz19thudem51n_3pg0i");

        GetDeepSearchResultsResultSet getinitialhouseResult = getinitialhouse.execute(getinitialhouseInput);

        String getinitialhouseresponse = getinitialhouseResult.get_Response();

        GetValuesFromXML getzpidvalue = new GetValuesFromXML(session);

        GetValuesFromXMLInputSet getzpidvalueInput = getzpidvalue.newInputSet();

        getzpidvalueInput.set_Node("zpid");
        getzpidvalueInput.set_XML(getinitialhouseresponse);

        GetValuesFromXMLResultSet getzpidvalueResult = getzpidvalue.execute(getzpidvalueInput);

        int zpid = Integer.parseInt(getzpidvalueResult.get_Result());

        com.temboo.Library.Zillow.GetComps getlistings = new com.temboo.Library.Zillow.GetComps(session);

        GetCompsInputSet getlistingsInput = getlistings.newInputSet();
        getlistingsInput.set_Count(5);
        getlistingsInput.set_ZPID(zpid);
        getlistingsInput.set_ZWSID("X1-ZWz19thudem51n_3pg0i");

        GetCompsResultSet getlistingsResult = getlistings.execute(getlistingsInput);

        String getlistingsresponse_str = getlistingsResult.get_Response();

        GetValuesFromXML getcompszpids = new GetValuesFromXML(session);
        GetValuesFromXMLInputSet getcompszpidsInput = getcompszpids.newInputSet();
        getcompszpidsInput.set_Node("zpid");
        getcompszpidsInput.set_XML(getlistingsresponse_str);
        getcompszpidsInput.set_ResponseFormat("csv");

        GetValuesFromXMLResultSet getcompszpidsResult = getcompszpids.execute(getcompszpidsInput);

        String comps_zpids_str = getcompszpidsResult.get_Result();

        GetValuesFromXML getcompsaddresses = new GetValuesFromXML(session);
        GetValuesFromXMLInputSet getcompsaddressesInput = getcompsaddresses.newInputSet();
        getcompsaddressesInput.set_Node("address");
        getcompsaddressesInput.set_XML(getlistingsresponse_str);
        getcompsaddressesInput.set_ResponseFormat("csv");

        GetValuesFromXMLResultSet getcompsaddressesResult = getcompsaddresses.execute(getcompsaddressesInput);

        String comps_addresses_str = getcompsaddressesResult.get_Result();

        List<String> comps_zpids = Arrays.asList(comps_zpids_str.split("\\s*,\\s*"));
        ArrayList<String> street_addresses = new ArrayList<String>();
        ArrayList<String> zipcodes = new ArrayList<String>();
        ArrayList<String> cities = new ArrayList<String>();
        ArrayList<String> states = new ArrayList<String>();
        ArrayList<String> latitudes = new ArrayList<String>();
        ArrayList<String> longitudes = new ArrayList<String>();

        JSONArray addresses = new JSONArray(comps_addresses_str);

        for (int i = 0; i < addresses.length(); i++)
        {
            JSONObject address = addresses.getJSONObject(i);

            street_addresses.add(address.getString("street"));
            zipcodes.add(address.getString("zipcode"));
            cities.add(address.getString("city"));
            states.add(address.getString("state"));
            latitudes.add(address.getString("latitude"));
            longitudes.add(address.getString("longitude"));
        }

        // Create markers
        LatLng posm1 = new LatLng(Double.parseDouble(latitudes.get(0)), Double.parseDouble(longitudes.get(0)));
        Marker m1 = GMap.addMarker(new MarkerOptions().position(posm1).title("House 1"));

        LatLng posm2 = new LatLng(Double.parseDouble(latitudes.get(1)), Double.parseDouble(longitudes.get(1)));
        Marker m2 = GMap.addMarker(new MarkerOptions().position(posm2).title("House 2"));

        LatLng posm3 = new LatLng(Double.parseDouble(latitudes.get(2)), Double.parseDouble(longitudes.get(2)));
        Marker m3 = GMap.addMarker(new MarkerOptions().position(posm3).title("House 3"));

        LatLng posm4 = new LatLng(Double.parseDouble(latitudes.get(3)), Double.parseDouble(longitudes.get(3)));
        Marker m4 = GMap.addMarker(new MarkerOptions().position(posm4).title("House 4"));

        LatLng posm5 = new LatLng(Double.parseDouble(latitudes.get(4)), Double.parseDouble(longitudes.get(4)));
        Marker m5 = GMap.addMarker(new MarkerOptions().position(posm5).title("House 5"));
    }
}
