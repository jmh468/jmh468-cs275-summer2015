package com.example.TicTacToe;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;

public class TicTacToeActivity extends Activity {
    /**
     * Called when the activity is first created.
     */

    // FOR THE GAME STATE:
    private boolean xTurn = true; // If this is true, it will be X's turn.
    private char boardArray[][] = new char[3][3]; // representing the board as a two dimensional array

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        initializeClickListeners();
        clearSpaces();
    }

    private void initializeClickListeners() {
        TableLayout TL = (TableLayout) findViewById(R.id.gridcontainer);
        for (int i = 0; i < TL.getChildCount(); i++)
        {
            if (TL.getChildAt(i) instanceof android.widget.TableRow)
            {
                TableRow TR = (TableRow) TL.getChildAt(i);
                for (int j = 0; j < TR.getChildCount(); j++)
                {
                    android.view.View button = TR.getChildAt(j);
                    button.setOnClickListener(new ExecuteMove(j, i));
                }
            }
        }
    }

    private class ExecuteMove implements View.OnClickListener {
        private int x = 0;
        private int y = 0;

        public ExecuteMove(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        @Override
        public void onClick(View v)
        {
            if (v instanceof android.widget.Button)
            {
                android.widget.Button activeSpace = (android.widget.Button) v;
                if (xTurn)
                {
                    boardArray[y][x] = 'X';
                }
                else
                {
                    boardArray[y][x] = 'O';
                }
                activeSpace.setText(xTurn ? "X" : "O");
                activeSpace.setEnabled(false);
                xTurn = !xTurn;

                if (declareWinner() != '\0')
                {
                    disableSpaces();
                }
            }
        }
    }

    public void clearBoard(View v)
    {
        xTurn = true;
        boardArray = new char[3][3];
        clearSpaces();
    }

    private void clearSpaces()
    {
        TableLayout TL = (TableLayout) findViewById(R.id.gridcontainer);
        for (int i = 0; i < TL.getChildCount(); i++)
        {
            if (TL.getChildAt(i) instanceof TableRow)
            {
                TableRow TR = (TableRow) TL.getChildAt(i);
                for (int j = 0; j < TR.getChildCount(); j++)
                {
                    if(TR.getChildAt(j) instanceof android.widget.Button)
                    {
                        android.widget.Button B = (android.widget.Button) TR.getChildAt(j);
                        B.setText("");
                        B.setEnabled(true);
                    }
                }
            }
        }
    }

    private boolean checkForWin(char[][] board, int size, char player)
    {
        for (int x = 0; x < size; x++) {
            int total = 0;
            for (int y = 0; y < size; y++) {
                if (boardArray[y][x] == player) {
                    total++;
                }
            }
            if (total >= size) {
                return true;
            }
        }

        for (int y = 0; y < size; y++) {
            int total = 0;
            for (int x = 0; x < size; x++) {
                if (boardArray[y][x] == player) {
                    total++;
                }
            }
            if (total >= size) {
                return true;
            }
        }

        int total = 0;
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                if (x == y && board[y][x] == player) {
                    total++;
                }
            }
        }
        if (total >= size) {
            return true;
        }

        total = 0;
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                if (x + y == size - 1 && boardArray[y][x] == player) {
                    total++;
                }
            }
        }
        if (total >= size) {
            return true;
        }

        return false;
    }

    private char declareWinner()
    {
        char triumphantwinner = '\0';

        if (checkForWin(boardArray, 3, 'X'))
        {
            triumphantwinner = 'X';
        }
        else if (checkForWin(boardArray, 3, '0'))
        {
            triumphantwinner = 'O';
        }

        android.widget.TextView trophyboard = (android.widget.TextView) findViewById(R.id.resultText);
        trophyboard.setText(triumphantwinner + " is the grand champion!");
        return triumphantwinner;
    }

    private void disableSpaces()
    {
        TableLayout TL = (TableLayout) findViewById(R.id.gridcontainer);
        for (int i = 0; i < TL.getChildCount(); i++)
        {
            if (TL.getChildAt(i) instanceof android.widget.TableRow)
            {
                TableRow TR = (TableRow) TL.getChildAt(i);
                for (int j = 0; j < TR.getChildCount(); j++)
                {
                    android.view.View button = TR.getChildAt(j);
                    button.setEnabled(false);
                }
            }
        }
    }
}
