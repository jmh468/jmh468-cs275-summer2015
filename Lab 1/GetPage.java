// Jason Hickey - Lab 1 - CS 275 Summer 2015
// 1. Warmup

import java.io.*;
import java.net.URL;
import java.net.HttpURLConnection;
import java.util.Scanner;
//import com.google.gson.JsonElement;
//import com.google.gson.JsonObject;
//import com.google.gson.JsonParser;

public class GetPage
{
	public static void main(String[] args)
	{
		try
		{
			URL sURL = new URL("https://www.cs.drexel.edu/~augenbdh/cs275_su15/labs/wxunderground.html");
			HttpURLConnection connection = (HttpURLConnection) sURL.openConnection();

			BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			String line;

			while((line = input.readLine()) != null)
			{
				System.out.println(line);
			}
			input.close();
		}//catch (HttpException e){
		 //	System.err.println("Fatal protocol violation: " + e.getMessage());
      	 //	e.printStackTrace();
	    catch (IOException e) {
	      System.err.println("Fatal transport error: " + e.getMessage());
	      e.printStackTrace();
	    }
	}
}