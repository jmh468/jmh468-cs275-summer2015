// Jason Hickey - Lab 1 - CS 275 Summer 2015

import java.io.*;
import java.net.URL;
import java.net.HttpURLConnection;
import java.util.Scanner;
import java.lang.Object;
import org.json.*;
//import com.google.gson.JsonElement;
//import com.google.gson.JsonObject;
//import com.google.gson.JsonParser;
//import org.json.*;

public class WeatherUnderground
{
	public static void main(String[] args) throws Exception
	{
		/// <GEOLOOKUP>
		URL urlGeo = new URL("http://api.wunderground.com/api/5358865cc04d1338/geolookup/q/PA/Philadelphia.json");
		
		HttpURLConnection requestGeo = (HttpURLConnection) urlGeo.openConnection();
		
		requestGeo.connect();
		
		BufferedReader dataInGeo = new BufferedReader(new InputStreamReader(urlGeo.openStream()));
		
		StringBuilder responseStringGeo = new StringBuilder();
		String lineGeo;
		
		while ((lineGeo = dataInGeo.readLine()) != null) {
			responseStringGeo.append(lineGeo);
		}
		
		JSONObject responseJSONGeoParent = new JSONObject(responseStringGeo.toString());
		JSONObject location = responseJSONGeoParent.getJSONObject("location");
		
		String zipcode = location.getString("zip");
		String city = location.getString("city");
		String state = location.getString("state");
		String latitude = location.getString("lat");
		String longitude = location.getString("lon");
		
		System.out.println("We will find the weather around Drexel University.");
		System.out.println("Zipcode: " + zipcode);
		System.out.println("City: " + city);
		System.out.println("State: " + state);
		System.out.println("Latitude: " + latitude);
		System.out.println("Longitude: " + longitude);
		System.out.println("\n");
		
		/// <HOURLY>
		URL urlHourly = new URL("http://api.wunderground.com/api/5358865cc04d1338/hourly/q/PA/Philadelphia.json");

		HttpURLConnection requestHourly = (HttpURLConnection) urlHourly.openConnection();

		requestHourly.connect();
		
		BufferedReader dataInHourly = new BufferedReader(new InputStreamReader(urlHourly.openStream()));
		
		StringBuilder responseStringHourly = new StringBuilder();
		String lineHourly;
		
		while ((lineHourly = dataInHourly.readLine()) != null) {
			responseStringHourly.append(lineHourly);
		}
		
		JSONObject responseJSONHourlyObject = new JSONObject(responseStringHourly.toString());
		JSONArray responseJSONHourly = responseJSONHourlyObject.getJSONArray("hourly_forecast");
		
		System.out.println("Hourly Forecast: ");
		
		for (Object item : responseJSONHourly) {
			JSONObject Date_Time = ((JSONObject)item).getJSONObject("FCTTIME");
			String month = Date_Time.getString("mon_padded");
			String day = Date_Time.getString("mday_padded");
			String hour = Date_Time.getString("hour");
			JSONObject tempJSON = ((JSONObject)item).getJSONObject("temp");
			String temp = tempJSON.getString("english");
			String current = ((JSONObject)item).getString("condition");
			String humidity = ((JSONObject)item).getString("humidity");
			
			System.out.println("===========================================================");
			System.out.println("Date: " + month + "/" + day + " || Time: " + hour + ":00");
			System.out.println("Weather: ");
			System.out.println("\tTemperature: " + temp);
			System.out.println("\tHumididty: " + humidity);
			System.out.println("\tCurrent Conditions: " + current);
			System.out.println("===========================================================");
		}
		
	}
}