import java.io.*;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.temboo.core.*;
import com.temboo.Library.Google.*;
import com.temboo.Library.Google.OAuth.*;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Google.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Google.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Google.Calendar.*;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsInputSet;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsResultSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsInputSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsResultSet;
import com.temboo.Library.CloudMine.*;
import com.temboo.Library.CloudMine.ObjectStorage.*;
import com.temboo.Library.CloudMine.ObjectStorage.ObjectGet.ObjectGetInputSet;
import com.temboo.Library.CloudMine.ObjectStorage.ObjectGet.ObjectGetResultSet;
import com.temboo.Library.CloudMine.ObjectStorage.ObjectSet.ObjectSetInputSet;
import com.temboo.Library.CloudMine.ObjectStorage.ObjectSet.ObjectSetResultSet;
import com.temboo.Library.CloudMine.UserAccountManagement.*;
import com.temboo.Library.CloudMine.UserAccountManagement.AccountLogin.AccountLoginInputSet;
import com.temboo.Library.CloudMine.UserAccountManagement.AccountLogin.AccountLoginResultSet;

public class CalendarCompare
{
	public static void main(String[] args) throws Exception
	{	
		final String CM_APP_ID = "a27a71c30b984e45f06a83833edb7de1";
		final String CM_API_KEY = "b2d1fe9452e64192bf07137871037c19";
		
		TembooSession session = new TembooSession("jasonhickey2", "GCalCloudmine-Lab5", "cXyDjKWjV5iwDXf6HaXNjHlXfZcJlGxx");
		
		InitializeOAuth gcalInitOAuth = new InitializeOAuth(session);
		
		InitializeOAuthInputSet gcalInitOAuthInputs = gcalInitOAuth.newInputSet();
		
		gcalInitOAuthInputs.setCredential("GCalCloudmine");
		gcalInitOAuthInputs.set_ClientID("72856698724-8h0mcij9catmaq634ej1ar64k6nt0nfv.apps.googleusercontent.com");
		gcalInitOAuthInputs.set_Scope("https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/calendar.readonly");
		
		InitializeOAuthResultSet gcalInitOAuthResults = gcalInitOAuth.execute(gcalInitOAuthInputs);
		
		System.out.println(gcalInitOAuthResults.get_AuthorizationURL());
		
		FinalizeOAuth gcalFinalOAuth = new FinalizeOAuth(session);
		
		FinalizeOAuthInputSet gcalFinalOAuthInputs = gcalFinalOAuth.newInputSet();
		
		gcalFinalOAuthInputs.setCredential("GCalCloudmine");
		gcalFinalOAuthInputs.set_CallbackID(gcalInitOAuthResults.get_CallbackID());
		gcalFinalOAuthInputs.set_ClientID("72856698724-8h0mcij9catmaq634ej1ar64k6nt0nfv.apps.googleusercontent.com");
		gcalFinalOAuthInputs.set_ClientSecret("2KOM9qY4cN_CSRR8XJjvx7nb");
		
		FinalizeOAuthResultSet gcalFinalOAuthResults = gcalFinalOAuth.execute(gcalFinalOAuthInputs);
		
		String accesstoken = gcalFinalOAuthResults.get_AccessToken();
		
		GetAllCalendars GCalgetcalendars = new GetAllCalendars(session);
		
		GetAllCalendarsInputSet GCalgetcalendarsInput = GCalgetcalendars.newInputSet();
		
		GCalgetcalendarsInput.set_AccessToken(accesstoken);
		GCalgetcalendarsInput.set_ResponseFormat("json");
		
		GetAllCalendarsResultSet GCalgetcalendarsResult = GCalgetcalendars.execute(GCalgetcalendarsInput);
		
		String getCalresponse = GCalgetcalendarsResult.get_Response();
		JSONObject getCalJSON = new JSONObject(getCalresponse);
		JSONArray calendars = getCalJSON.getJSONArray("items");
		ArrayList<String> calendartitles = new ArrayList<String>();
		ArrayList<String> calendarIDs = new ArrayList<String>();
		
		System.out.println("Calendars found: ");
		
		for (int i = 0; i < calendars.length(); i++)
		{
			JSONObject calendar = calendars.getJSONObject(i);
			String calendarname = calendar.getString("summary");
			String calendarID = calendar.getString("id");
			calendartitles.add(calendarname);
			calendarIDs.add(calendarID);
			System.out.println("\t" + calendarname);
		}
		
		System.out.println("==================================");
		
		
		GetAllEvents GCalgetevents = new GetAllEvents(session);
		GetAllEventsInputSet GCalgeteventsInput = GCalgetevents.newInputSet();
		GCalgeteventsInput.set_AccessToken(accesstoken);
		GCalgeteventsInput.set_ResponseFormat("json");
		GCalgeteventsInput.set_ClientID("72856698724-8h0mcij9catmaq634ej1ar64k6nt0nfv.apps.googleusercontent.com");
		GCalgeteventsInput.set_ClientSecret("2KOM9qY4cN_CSRR8XJjvx7nb");
		
		AccountLogin cloudminelogin = new AccountLogin(session);
		
		AccountLoginInputSet cloudmineloginInputs = cloudminelogin.newInputSet();
		
		cloudmineloginInputs.set_APIKey(CM_API_KEY);
		cloudmineloginInputs.set_ApplicationIdentifier(CM_APP_ID);
		cloudmineloginInputs.set_Username("jmh468@drexel.edu");
		cloudmineloginInputs.set_Password("test");
		cloudmineloginInputs.setCredential("CloudmineUserAuth");
		
		AccountLoginResultSet cloudmineloginResult = cloudminelogin.execute(cloudmineloginInputs);
		
		JSONObject loginresponse = new JSONObject(cloudmineloginResult.get_Response());
		String sessiontoken = loginresponse.getString("session_token");
		
		int counter = 0;
		ObjectSet createcloudmineobj = new ObjectSet(session);
		ObjectSetInputSet createcloudmineobjInput = createcloudmineobj.newInputSet();
		createcloudmineobjInput.set_APIKey(CM_API_KEY);;
		createcloudmineobjInput.set_ApplicationIdentifier(CM_APP_ID);
		createcloudmineobjInput.setCredential("CloudmineUserAuth");
		createcloudmineobjInput.set_SessionToken(sessiontoken);
		
		for (Object id : calendarIDs)
		{	
			GCalgeteventsInput.set_CalendarID(id.toString());
			
			GetAllEventsResultSet GCalgeteventsResult = GCalgetevents.execute(GCalgeteventsInput);
			
			String getEventsresponse = GCalgeteventsResult.get_Response();
			
			JSONObject getEventsJSON = new JSONObject(getEventsresponse);
			JSONArray events = getEventsJSON.getJSONArray("items");
			
			System.out.println("Events in " + calendartitles.get(counter).toString() + ": " + events.length());
			System.out.println("Events: ");
			
			for (int i = 0; i < events.length(); i++)
			{
				JSONObject eventobject = events.getJSONObject(i);
				String title = eventobject.getString("summary");
				JSONObject startobject = eventobject.getJSONObject("start");
				String start = startobject.getString("dateTime");
				JSONObject endobject = eventobject.getJSONObject("end");
				String end = endobject.getString("dateTime");
				
				System.out.println("=======================================");
				System.out.println("Title: " + title);
				System.out.println("Start date/time: " + start);
				System.out.println("End date/time: " + end);
				System.out.println("=======================================");
				
				// construct JSON for cloudmine
				JSONObject jso = new JSONObject();
				jso.put("username", "jmh468@drexel.edu");
				jso.put("calendar_id", id);
				jso.put("google_token", accesstoken);
				
				String jsondata = jso.toString();
				createcloudmineobjInput.set_Data(jsondata);
				
				ObjectSetResultSet createobjResult = createcloudmineobj.execute(createcloudmineobjInput);
				
				String resultofwrite = createobjResult.get_Response();
				JSONObject resultJSON = new JSONObject(resultofwrite);
				JSONObject success = resultJSON.getJSONObject("success");
				if (success != null)
				{
					System.out.println("Write successful");
				}
			}
			
			counter++;
		}
		
		// Read from cloudmine
		ObjectGet readCM = new ObjectGet(session);
		
		ObjectGetInputSet readCMInput = readCM.newInputSet();
		readCMInput.set_APIKey(CM_API_KEY);
		readCMInput.set_ApplicationIdentifier(CM_APP_ID);
		readCMInput.set_Limit(-1);
		readCMInput.set_SessionToken(sessiontoken);
		
		ObjectGetResultSet readCMResult = readCM.execute(readCMInput);
		
		JSONObject readCMResponseJSON = new JSONObject(readCMResult.get_Response());
		JSONObject result = readCMResponseJSON.getJSONObject("success");
		
		System.out.println("Objects: ");
		System.out.println(result.toString());
		
	}
}
